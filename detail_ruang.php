<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "ujikom";
 
    // membuat koneksi
    $koneksi = new mysqli($servername, $username, $password, $dbname);
 
    // melakukan pengecekan koneksi
    if ($koneksi->connect_error) {
        die("Connection failed: " . $koneksi->connect_error);
    } 
 
    if($_POST['rowid']) {
        $id_ruang = $_POST['rowid'];
        // mengambil data berdasarkan id
    $sql= "select * from ruang where id_ruang=$id_ruang";
	$result=$koneksi->query($sql);
	foreach ($result as $baris){
		?>
            <table class="table table-striped responsive-utilities jambo_table bulk_action">
                <tr>
                    <td>Nama Ruangan</td>
                    <td>:</td>
                    <td><?php echo $baris['nama_ruang']; ?></td>
                </tr>
                <tr>
                    <td>Kode Ruang</td>
                    <td>:</td>
                    <td><?php echo $baris['kode_ruang']; ?></td>
                </tr>
				<tr>
                    <td>Keterangan</td>
                    <td>:</td>
                    <td><?php echo $baris['keterangan']; ?></td>
                </tr>
            </table>
        <?php 
 
        }
    }
    $koneksi->close();
?>