<?php
    session_start();
	include "koneksi.php";
	?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>INVENTARIS| </title>

        <!-- Bootstrap core CSS -->

        <link href="css/bootstrap.min.css" rel="stylesheet">

        <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="css/custom.css" rel="stylesheet">
        <link href="css/icheck/flat/green.css" rel="stylesheet">

        <link href="css/calendar/fullcalendar.css" rel="stylesheet">
        <link href="css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">

        <script src="js/jquery.min.js"></script>

        <!--[if lt IE 9]>
            <script src="../assets/js/ie8-responsive-file-warning.js"></script>
            <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

   
</head>

<body>
    

    <div class="container"><br></br>
        <div class="row"><br></br>
            <div class="col-md-4 col-md-offset-4"><br></br>
                <div class="login-panel panel panel-default"><br></br>
                    <div class="panel-heading">
					 <li><a href="index.php"><i>Kembali ke Dashbord</i></a>
                        <h3 class="panel-title">Please Sign In Terlebih Dahulu</h3>
                    </div>
                    <div class="panel-body">
                        <form action="cek_login.php" method="post">
										<div class="form-group">
                                            <label>Username</label>
                                            <input type="text" name="username" autocomplete="off" class="form-control" required>
                                        </div>
										<div class="form-group">
                                            <label>Password</label>
                                            <input type="password" name="password" autocomplete="off" class="form-control" required>
                                        </div>
										<div class="form-group">
                                   
                                        <input type="submit" name="submit" value="Sign In" class="btn btn-lg btn-success btn-block">
                                   
                                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
